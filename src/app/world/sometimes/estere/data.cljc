(ns world.sometimes.estere.data
  (:require
   [clojure.spec.alpha :as spec]
   [world.sometimes.estere.data.macros :refer [variant]]))


;; a path is a variant containing a sequence of file names
;; [::path ["src" "world" "sometimes"]]
(spec/def ::path
  (variant ::path (spec/coll-of string?)))


;; a file is a variant containing an absolute file path
;; [::file "C:\\Users" :directory]
(spec/def ::file
  (variant ::file string? #{:file :directory}))


;; a file set is variant containing a collection of distinct files
(spec/def ::file-set
  (variant ::file-set (spec/coll-of ::file :distinct true)))

