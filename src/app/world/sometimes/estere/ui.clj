(ns world.sometimes.estere.ui)


(defmacro <p!
  "EXPERIMENTAL: Takes the value of a promise resolution. The value of a rejected promise
  will be thrown wrapped in a instance of ExceptionInfo, acessible via ex-cause.

  See cljs.core.async ns."
  [exp]
  `(let [v# (clojure.core.async/<! (world.sometimes.estere.ui/js-promise->chan ~exp))]
     (if (and (instance? clojure.core/ExceptionInfo v#)
              (= (ex-message v#) "Promise error"))
       (throw v#)
       v#)))
