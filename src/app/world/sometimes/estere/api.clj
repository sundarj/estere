(ns world.sometimes.estere.api
  (:require
   [clojure.java.io]
   [clojure.string]
   [world.sometimes.estere.data :as estere.data]))


(defn ->file [file-path file-or-directory]
  [::estere.data/file file-path file-or-directory])

(comment
  (->file "." :directory)
  (->file "docs/spec.org" :file)
  )


(defn ->file-set [files]
  [::estere.data/file-set files])


(defn java-path->file [^java.nio.file.Path java-path]
  (let [java-file (.toFile java-path)]
    (->file (.getCanonicalPath java-file) (if (.isDirectory java-file) :directory :file))))

(comment
  (java-path->file (clojure.java.io/file "."))
  (java-path->file (clojure.java.io/file "src"))
  )


(defn -main [& args]
  (with-open [dir-stream (java.nio.file.Files/newDirectoryStream
                          (java.nio.file.Paths/get "." (into-array String [])))]
    (->file-set (into [] (map java-path->file) dir-stream))))


(comment
  (file-seq (clojure.java.io/file "docs"))
  (-main)
  )
