(ns world.sometimes.estere.data.macros
  (:require
   [clojure.spec.alpha :as spec])
  #?(:cljs
     (:require-macros
      [world.sometimes.estere.data.macros])))

(defmacro variant
  "A variant is a tagged tuple, like [:file \"foo.txt\"]"
  [name & args]
  `(spec/tuple #{name} ~@args))
