(ns world.sometimes.estere.ui
  (:require
   [cognitect.anomalies :as anom]
   [com.fulcrologic.fulcro.application :as app]
   [com.fulcrologic.fulcro.components :as comp]
   [com.fulcrologic.fulcro.dom :as dom]
   [clojure.core.async :as async]
   [clojure.datafy :as datafy]
   [clojure.core.protocols :as protocols]
   [clojure.repl :as repl]
   [goog.object]
   [world.sometimes.estere.data :as data])
  (:require-macros
   [world.sometimes.estere.ui :refer [<p!]]))


(defn js-promise->chan
  "EXPERIMENTAL: Puts the promise resolution into a promise-chan and returns it.
   The value of a rejected promise will be wrapped in a instance of
   ExceptionInfo, acessible via ex-cause.

  See cljs.core.async.interop ns."
  [p]
  (let [c (async/promise-chan)]
    (.then p
           (fn [res]
             (if (nil? res)
               (async/close! c)
               (async/put! c res)))
           (fn [err]
             (async/put! c (ex-info "Promise error"
                                    {::anom/category ::anom/fault}
                                    err))))
    c))


(defn async-iterable->chan [o]
  (let [c (async/chan)
        ai (js-invoke o (.-asyncIterator js/Symbol))]
    (async/go-loop []
      (let [v (<p! (.next ai))]
        (if (.-done v)
          (async/close! c)
          (do (>! c (.-value v))
              (recur)))))
    c))


(defonce taps (delay (add-tap #(.log js/console %))))
(force taps)


(comp/defsc Estere [instance {:keys [:world.sometimes.estere/ui]}]
  {:ident (fn [] [:components/by-name `Estere])
   :initial-state (fn [_] {:world.sometimes.estere/ui {}})
   :query [:world.sometimes.estere/ui]
   :initLocalState (fn [instance props]
                     {:dir-name nil
                      :entry-name->handle {}})}
  (let [{:keys [dir-name entry-name->handle]} (comp/get-state instance)
        entries (keys entry-name->handle)
        open-folder (fn [_]
                      (async/go
                        (try
                          (let [handle (<p! (.showDirectoryPicker js/window))
                                entries-ch (async-iterable->chan handle)
                                entries (<! (async/into [] entries-ch))
                                entry-name->handle (into {} (map vec) entries)]
                            (comp/set-state!
                             instance
                             {:dir-name (.-name handle)
                              :entry-name->handle entry-name->handle}))
                          (catch ExceptionInfo _ _))))
        open-entry (fn [event]
                     (async/go
                       (try
                         (let [entry-name (goog.object/getValueByKeys event
                                                                      "target"
                                                                      "dataset"
                                                                      "entry")]
                           (if-let [handle (entry-name->handle entry-name)]
                             (if (= "directory" (goog.object/get handle
                                                                 "kind"))
                               (let [entries-ch (async-iterable->chan (.entries handle))
                                     entries (<! (async/into [] entries-ch))]
                                 (.log js/console entries))))))))]
    (dom/main {:className "world-sometimes-estere"}
              (dom/header
               (dom/button {:onClick open-folder} "Open folder"))
              (if dir-name
                (comp/fragment
                 (dom/h1 dir-name)
                 (if-not (seq entries)
                   (dom/p "It's empty.")
                   (apply dom/ul (for [entry entries]
                                   (dom/li (dom/button {:type "button"
                                                        :onClick open-entry
                                                        :data-entry entry}
                                                       entry))))))))))


(def ui-estere (comp/factory Estere))


(comp/defsc FulcroRoot [instance db]
  {:initial-state (fn [_] {:ui/root (comp/get-initial-state Estere)})
   :query [{:ui/root (comp/get-query Estere)}]}
  (ui-estere (:ui/root db)))


(defonce fulcro-app (app/fulcro-app))


(defn ^:export start
  "Shadow-cljs sets this up to be our entry-point function. See shadow-cljs.edn
  `:init-fn` in the modules of the main build."
  []
  (app/mount! fulcro-app FulcroRoot "mount-point"))


(defn ^:export refresh
  "During development, shadow-cljs will call this on every hot reload of source.
  See shadow-cljs.edn"
  []
  ;; re-mounting will cause forced UI refresh, update internals, etc.
  (app/mount! fulcro-app FulcroRoot "mount-point"))


(comment
  (js/alert 1)
  (start)
  (repl/dir clojure.core.protocols)
  (repl/dir datafy))
