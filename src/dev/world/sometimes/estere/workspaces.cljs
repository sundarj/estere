(ns world.sometimes.estere.workspaces
  (:require
   [clojure.spec.test.alpha :as spec.test]
   [com.fulcrologic.fulcro.routing.dynamic-routing :as routing]
   [nubank.workspaces.core :as workspaces]
   [nubank.workspaces.card-types.react :as card-types.react]
   [nubank.workspaces.card-types.fulcro3 :as card-types.fulcro]
   [nubank.workspaces.model :as workspaces.model]
   [world.sometimes.estere.ui :as estere.ui]))


#_(spec.test/instrument)


(workspaces/defcard estere
  (card-types.fulcro/fulcro-card
   {::card-types.fulcro/root estere.ui/Estere}))
